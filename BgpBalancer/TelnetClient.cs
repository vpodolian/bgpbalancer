using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace BgpBalancer
{
    enum Verbs
    {
        WILL = 251,
        WONT = 252,
        DO = 253,
        DONT = 254,
        IAC = 255
    }

    enum Options
    {
        SGA = 3
    }

    class TelnetClient
    {
        TcpClient tcpSocket;

        int TimeOutMs = 100;

        public TelnetClient(string Hostname, int Port)
        {
            tcpSocket = new TcpClient(Hostname, Port);

        }

        public string Login(string Username, string Password, int LoginTimeOutMs)
        {
            int oldTimeOutMs = TimeOutMs;
            TimeOutMs = LoginTimeOutMs;
            string s = Read();
            if (!s.TrimEnd().EndsWith(":"))
                throw new Exception("Failed to connect : no login prompt");
            WriteLine(Username);

            s += Read();
            if (!s.TrimEnd().EndsWith(":"))
                throw new Exception("Failed to connect : no password prompt");
            WriteLine(Password);

            s += Read();
            TimeOutMs = oldTimeOutMs;
            return s;
        }

        public void WriteLine(string cmd)
        {
            Write(cmd + "\r");
        }

        public void Write(string cmd)
        {
            if (!tcpSocket.Connected) return;
            byte[] buf = System.Text.ASCIIEncoding.ASCII.GetBytes(cmd.Replace("\0xFF", "\0xFF\0xFF"));
            tcpSocket.GetStream().Write(buf, 0, buf.Length);
        }

        public string Read()
        {
            if (!tcpSocket.Connected) return null;
            StringBuilder sb = new StringBuilder();
            do
            {
                ParseTelnet(sb);
                System.Threading.Thread.Sleep(TimeOutMs);
            } while (tcpSocket.Available > 0);
            return sb.ToString();
        }

        public void SetPrepend(int deviceAs, string neighborIp, string routeMapName, int count)
        {
            if (!IsConnected)
                return;
            
            var prependBuilder = new StringBuilder();
            for (int i = 0; i < count; i++)
                prependBuilder.AppendFormat("{0} ", deviceAs);

            WriteLine("!");
            if (GetPrompt() == ">")
                WriteLine("enable");
            WriteLine("config terminal");
            WriteLine("router bgp " + deviceAs);    // ��������� BGP
            // ��������� route-map ��� �������� AS
            WriteLine(string.Format("neighbor {0} route-map {1} out", neighborIp, routeMapName));
            // ����������� ��������� route-map
            WriteLine(string.Format("route-map {0} permit 10", routeMapName));
            // ������������� AS-PATH prepend
            WriteLine(string.Format("set as-path prepend {0}", prependBuilder.ToString()));
            WriteLine("end");
        }

        public int GetPrepends(string routeMap)
        {
            if (!IsConnected)
                return 0;
            WriteLine("");
            var prompt = GetPrompt();
            if (prompt == ">")
                WriteLine("enable");
            WriteLine(string.Format("show route-map {0} | include prepend", routeMap));
            var prependString = Read();
            int pos = prependString.IndexOf("prepend");
            if (pos > -1)
            {
                int prepends;
                if (int.TryParse(prependString.Substring(prependString.Length - 1, pos).Trim(), out prepends))
                    return prepends;
            }
            return 0;
        }

        public bool IsConnected
        {
            get { return tcpSocket.Connected; }
        }

        public string GetPrompt()
        {
            if (!IsConnected)
                return string.Empty;
            var str = Read();
            str.TrimEnd(); 
            return str.Substring(str.Length - 1, 1);
        }

        void ParseTelnet(StringBuilder sb)
        {
            while (tcpSocket.Available > 0)
            {
                int input = tcpSocket.GetStream().ReadByte();
                switch (input)
                {
                    case -1:
                        break;
                    case (int)Verbs.IAC:
                        // interpret as command
                        int inputverb = tcpSocket.GetStream().ReadByte();
                        if (inputverb == -1) break;
                        switch (inputverb)
                        {
                            case (int)Verbs.IAC:
                                //literal IAC = 255 escaped, so append char 255 to string
                                sb.Append(inputverb);
                                break;
                            case (int)Verbs.DO:
                            case (int)Verbs.DONT:
                            case (int)Verbs.WILL:
                            case (int)Verbs.WONT:
                                // reply to all commands with "WONT", unless it is SGA (suppres go ahead)
                                int inputoption = tcpSocket.GetStream().ReadByte();
                                if (inputoption == -1) break;
                                tcpSocket.GetStream().WriteByte((byte)Verbs.IAC);
                                if (inputoption == (int)Options.SGA)
                                    tcpSocket.GetStream().WriteByte(inputverb == (int)Verbs.DO ? (byte)Verbs.WILL : (byte)Verbs.DO);
                                else
                                    tcpSocket.GetStream().WriteByte(inputverb == (int)Verbs.DO ? (byte)Verbs.WONT : (byte)Verbs.DONT);
                                tcpSocket.GetStream().WriteByte((byte)inputoption);
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        sb.Append((char)input);
                        break;
                }
            }
        }
    }
}
