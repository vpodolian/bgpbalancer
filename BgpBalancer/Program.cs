﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Lextm.SharpSnmpLib;
using Lextm.SharpSnmpLib.Messaging;

namespace BgpBalancer
{
    class Program
    {
        static int deltaMbps = 200;
        static int interval = 10;
        static Device device;
        static void Main(string[] args)
        {
            // Конфигурация
            Configure();

            Logger.LogMsg(string.Format("IP: {0}, кол-во интерфейсов: {1}", device.Ip, device.Ports.Count));
            // Сбор ланных о трафике на портах
            SNMPClient.MeasureTraffic(device, interval);
            // Анализ
            var prepends = Analyze();
            if (prepends.Count > 0)
            {
                try
                {
                ChangeRoutes(prepends); // изменяем маршруты
        }
                catch (Exception e)
                {
                    Logger.LogError(e.Message);
                }
            }
        }

        public static IDictionary<int, bool> Analyze()
        {
            var routeTasks = new Dictionary<int, bool>(); // номер порта, true - добавить prepend
            if (device == null && device.Ports.Count > 1)
                return routeTasks; // если нет ссылки на объект или нет данных о портах - выходим
            // Вычисляем среднее значение
            var sum = 0;
            foreach (var port in device.Ports)
                sum += port.Traffic;
            int average = sum / device.Ports.Count;
            if (average == 0)
                return routeTasks;
            // Проверяем отклонения от среднего
            foreach (var port in device.Ports)
            {
                if (Math.Abs(average - port.Traffic) > deltaMbps)
                {
                    // Снизить трафик
                    routeTasks.Add(port.Number, true);
                }
                else if (Math.Abs(average - port.Traffic) < deltaMbps)
                {
                    // Повысить трафик
                    routeTasks.Add(port.Number, false);
                }
            }
            return routeTasks;
        }

        public static void ChangeRoutes(IDictionary<int, bool> prepends)
        {
			var tc = new TelnetClient(device.Ip, 2101);
            // Создаем Telnet-соединение и подключаемся
            string s = tc.Login("root", "rootpassword", 100);
            // Получаем ответ о сервера
            string prompt = tc.GetPrompt();
            if (prompt != "$" && prompt != ">")
                throw new Exception("Connection failed");
            prompt = "";
            tc.WriteLine("enable");
            prompt = tc.GetPrompt();
            if (prompt != "#")
                throw new Exception("Запрещен доступ в режим #!");
            // Изменяем маршруты
            foreach (var prep in prepends)
            {
                // Нужно узнать текущее количество препендов в route-map
                int prependCount = tc.GetPrepends(device.Ports[prep.Key].RouteMap);
                if (prependCount == 0)
                    continue;
                // Увеличиваем или уменьшаем prepend
                if (prep.Value)
                    prependCount++;
                else
                    prependCount--;
                tc.SetPrepend(device.AS, device.Ports[prep.Key].NeighborIp, device.Ports[prep.Key].RouteMap, prependCount);
            }
        }

        private static void Configure()
        {
            var ipString = ConfigurationManager.AppSettings["ip"];
            IPAddress ip;
            if (!IPAddress.TryParse(ipString, out ip))
            {
                Logger.LogError("IP-адрес в конфигурационном файле имеет неверный формат");
                Console.ReadKey();
                return;
            }
            deltaMbps = ConfigurationManager.AppSettings["deltaMbps"] != null ? int.Parse(ConfigurationManager.AppSettings["deltaMbps"]) : 200;

            // Создаем объект устройства
            device = new Device(ip) { Community = "public" };
            // Узнать количество портов            
            var ifCount = SNMPClient.GetPortsCount(device);
            if (ifCount == 0)
                return;

            device.AS = int.Parse(ConfigurationManager.AppSettings["as"]);

            var doc = XElement.Load("neighbors.xml");
            var elements = doc.Elements("link");
            for (int i = 1; i <= ifCount; i++)
            {
                if (elements.Any(e => int.Parse(e.Attribute("port").Value) == i))
                {
                    // Получаем IP соседней AS из файла настроек
                    var link = elements.Single(e => int.Parse(e.Attribute("port").Value) == i);
                    device.Ports.Add(new Port(i) { NeighborIp = link.Attribute("neighbor").Value });
                }
            }
        }
    }
}
