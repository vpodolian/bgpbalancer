﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lextm.SharpSnmpLib;
using Lextm.SharpSnmpLib.Messaging;
using System.Net;

namespace BgpBalancer
{
    public static class SNMPClient
    {
        public static int GetPortsCount(Device device)
        {
            var result = Messenger.Get(VersionCode.V1,      // get-запрос ifCount
                            new IPEndPoint(device.Ip, 161),
                            new OctetString(device.Community),
                            new List<Variable> { new Variable(new ObjectIdentifier("1.3.6.1.2.1.2.1.0")) },
                            60000);
            if (result.Count == 0)
            {
                Console.WriteLine("Не удалось получить количество интерфейсов");
                Console.ReadKey();
                return 0;
            }
            return int.Parse(result[0].Data.ToString());
        }

        public static IList<Variable> GetInOctets(Device device)
        {
            var oidList = new List<Variable>(); // список OID для запроса SNMP-get
            for (int i = 1; i <= device.Ports.Count; i++)
            {
                oidList.Add(new Variable(new ObjectIdentifier("1.3.6.1.2.1.2.2.1.10." + i)));
            }
            return Messenger.Get(VersionCode.V1,
                        new IPEndPoint(device.Ip, 161),
                        new OctetString(device.Community),
                        oidList,
                        60000);
        }

        public static void MeasureTraffic(Device device, int t)
        {
            // Для каждого интерфейса определить значение ifInOctets
            var values = SNMPClient.GetInOctets(device);
            if (values.Count > 0)	// если получили значения
            {
                for (int i = 0; i < values.Count; i++)
                {
					// Перевести строковое значение в целое число
                    int InOctets = int.Parse(values[i].Data.ToString());
                    // Заполнить поля объекта
                    device.Ports[i].Traffic = (InOctets - device.Ports[i].LastInOctets) / t; // расчет трафика
                    device.Ports[i].LastInOctets = InOctets;
                    Logger.LogMsg(string.Format("ifNum: {0}, ifInOctets: {1}, traffic: {2}",
                        device.Ports[i].Number, device.Ports[i].LastInOctets, device.Ports[i].Traffic));
                }
            }
        }
    }
}
