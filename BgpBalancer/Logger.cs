﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace BgpBalancer
{
    public static class Logger
    {
        static string logDir;
        public static bool DebugMode {get; set;}
        static Logger()
        {
            logDir = ConfigurationManager.AppSettings["logDir"] != null ? ConfigurationManager.AppSettings["logDir"] : "";
        }

        /// <summary>
        /// Записывает информационное сообщение в лог 
        /// </summary>
        /// <param name="text">текст сообщения</param>
        public static void LogMsg(string text)
        {
            using (var file = System.IO.File.AppendText(logDir + "log.txt"))
            {
                if (DebugMode)
                    Console.WriteLine(string.Format("{0} - {1}", DateTime.Now, text));
                try
                {
                    file.WriteLine(string.Format("{0} - {1}", DateTime.Now, text));
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("{0} - ERROR - Ошибка записи в файл лога! Причина: {1}", DateTime.Now, e.Message));
                }
            }
        }

        /// <summary>
        /// Записывает сообщение об ошибке в лог
        /// </summary>
        /// <param name="error">описание ошибки</param>
        public static void LogError(string error)
        {
            using (var file = System.IO.File.AppendText(logDir + "log.txt"))
            {
                if (DebugMode)
                    Console.WriteLine(string.Format("{0} - ERROR - {1}", DateTime.Now, error));
                try
                {
                    file.WriteLine(string.Format("{0} - ERROR - {1}", DateTime.Now, error));
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("{0} - ERROR - Ошибка записи в файл лога! Причина: {1}", DateTime.Now, e.Message));
                }
            }
        }
    }
}
