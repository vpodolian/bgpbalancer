﻿using System;
using System.Collections.Generic;
using System.Net;

namespace BgpBalancer
{
    public class Device
    {
        public IPAddress Ip { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Community { get; set; }
        public int AS { get; set; }
        public List<Port> Ports { get; set; }

        public Device(IPAddress ip)
        {
            Ip = ip;
            Ports = new List<Port>();
        }
    }
}
