﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BgpBalancer
{
    public class Port
    {
        public int Number { get; private set;}
        public int LastInOctets { get; set; }
        public int Traffic { get; set; }
        public string NeighborIp { get; set; }
        public string RouteMap { get; set; }

        public Port(int num)
        {
            Number = num;
        }
    }
}
